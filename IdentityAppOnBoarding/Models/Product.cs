﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IdentityAppOnBoarding.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }

        [DisplayName("Image Name")]
        public string Image { get; set; } = string.Empty;
        public int Stock { get; set; }
        public double Price { get; set; }
        public DateTime LastUpdate { get; set; }
        public virtual Category Category { get; set; }

        [NotMapped]
        [DisplayName("Upload File")]
        public IFormFile ImageFile { get; set; }

        public ICollection<Cart> Carts { get; set; }
    }
}
