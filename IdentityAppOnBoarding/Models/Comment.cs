﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IdentityAppOnBoarding.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("AppUser")]
        public string UserId { get; set; }
        public string Message { get; set; }
        public virtual AppUser? AppUser { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
