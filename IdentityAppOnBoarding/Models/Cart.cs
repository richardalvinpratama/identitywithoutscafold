﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IdentityAppOnBoarding.Models
{
    public class Cart
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        [ForeignKey("AppUser")]
        public string UserId { get; set; }
        public int Qty { get; set; }
        public virtual AppUser AppUser { get; set; }
        public virtual Product Product { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
