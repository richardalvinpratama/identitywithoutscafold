using IdentityAppOnBoarding.Data;
using IdentityAppOnBoarding.Helpers;
using IdentityAppOnBoarding.Interfaces;
using IdentityAppOnBoarding.Models;
using IdentityAppOnBoarding.Services;
using IdentityAppOnBoarding.Services.Contracts;
using IdentityAppOnBoarding.Services.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<ApplicationDbContext>(e =>
    e.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddIdentity<AppUser, IdentityRole>().AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();

builder.Services.AddControllersWithViews();

//Helpers AuthMessageSenderOtions
builder.Services.Configure<AuthMessageSenderOptions>(builder.Configuration.GetSection("SendGrid"));

//Send email 
builder.Services.AddTransient<ISendGridEmail, SendGridEmail>();

//Services for custom policy rule
/*builder.Services.AddAuthorization(options =>
{
options.AddPolicy("OnlyTrainerChecker", policy => policy.Requirements.Add(new OnlyTrainerAuthorization())));
});*/

//Interfaces
builder.Services.AddScoped<ICategoryServices, CategoryServices>();
builder.Services.AddScoped<IProductServices, ProductServices>();
builder.Services.AddScoped<ICartServices, CartServices>();
builder.Services.AddScoped<ICommentServices, CommentServices>();

//Google OAuth
builder.Services.AddAuthentication().AddGoogle(options =>
{
    options.ClientId = "test";
    options.ClientSecret = "test";
});

builder.Services.Configure<IdentityOptions>(opt =>
{
    opt.Password.RequiredLength = 5;
    opt.Password.RequireLowercase = true;
    opt.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromSeconds(10);
    opt.Lockout.MaxFailedAccessAttempts = 5;
/*    opt.SignIn.RequireConfirmedAccount = true;*/
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
