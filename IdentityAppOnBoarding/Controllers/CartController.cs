﻿using IdentityAppOnBoarding.Models;
using IdentityAppOnBoarding.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace IdentityAppOnBoarding.Controllers
{
    public class CartController : Controller
    {
        private readonly ICartServices _cartServices;
        private readonly UserManager<AppUser> _userManager;
        private readonly IProductServices _productServices;
        public CartController(UserManager<AppUser> userManager, ICartServices cartServices, IProductServices productServices)
        {
            _cartServices = cartServices;
            _userManager = userManager;
            _productServices = productServices;
        }
        public IActionResult Index()
        {
            return View(_cartServices.GetCartsbyUserId(_userManager.GetUserId(HttpContext.User)));
        }
        public IActionResult IndexAdmin()
        {
            return View(_cartServices.GetCarts());
        }
        public IActionResult CreateAdmin()
        {
            ViewBag.LinkText = "Cart";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAdmin(Cart obj)
        {
            _cartServices.CreateCart(obj);
            TempData["success"] = "Cart Added successfully!";
            return RedirectToAction("IndexAdmin");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(int Id)
        {
            var cart = new Cart
            {
                UserId = _userManager.GetUserId(HttpContext.User),
                Qty = 1,
                ProductId = Id,
            };

            _cartServices.CreateCart(cart);
            TempData["success"] = "Cart Added successfully!";
            return RedirectToAction("Index");
        }

        public IActionResult Update(int id, int Qty)
        {
            ViewBag.LinkText = "Product";
            if (_cartServices.CartExists(id))
            {
                var cart = _cartServices.GetCart(id);
                cart.Qty = Qty;
                _cartServices.UpdateCart(cart);
                return RedirectToAction("Index");
            }
            return NotFound();
        }

        public IActionResult DeleteAdmin(int id)
        {
            if (_cartServices.CartExists(id))
            {
                var cart = _cartServices.GetCart(id);
                TempData["success"] = "Cart delete successfully!";
                _cartServices.DeleteCart(cart);
                return RedirectToAction("IndexAdmin");
            }
            return NotFound();
        }

        public IActionResult Delete(int id)
        {
            if (_cartServices.CartExists(id))
            {
                var cart = _cartServices.GetCart(id);
                TempData["success"] = "Cart delete successfully!";
                _cartServices.DeleteCart(cart);
                return RedirectToAction("Index");
            }
            return NotFound();
        }
    }
}
