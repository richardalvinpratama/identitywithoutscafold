﻿using IdentityAppOnBoarding.Models;
using IdentityAppOnBoarding.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace IdentityAppOnBoarding.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICategoryServices _categoryServices;
        private readonly IProductServices _productServices;

        public HomeController(ILogger<HomeController> logger, ICategoryServices categoryServices, IProductServices productServices)
        {
            _logger = logger;
            _categoryServices = categoryServices;
            _productServices = productServices;
        }

        public IActionResult Index()
        {
            return View(_categoryServices.GetCategories());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Admin()
        {
            ViewBag.LinkText = "AdminDashboard";
            return View();
        }

        public IActionResult AllCategory()
        {
            return View();
        }

        public async Task<IActionResult> ProductByCategory(int id, string currentFilter, string searchString, int? pageNumber)
        {
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            var products = _productServices.GetProductbyCategory(id);

            if (!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(p => p.Name.Contains(searchString));
            }

            ViewBag.LinkText = "Product";

            int pageSize = 3;
            return View(await PaginatedList<Product>.CreateAsync(products, pageNumber ?? 1, pageSize));
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}