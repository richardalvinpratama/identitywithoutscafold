﻿using IdentityAppOnBoarding.Models;
using IdentityAppOnBoarding.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace IdentityAppOnBoarding.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentServices _commentServices;
        private readonly UserManager<AppUser> _userManager;
        public CommentController(UserManager<AppUser> userManager, ICommentServices commentServices)
        {
            _commentServices = commentServices;
            _userManager = userManager;
        }
        public IActionResult Index()
        {
            return View(_commentServices.GetComments());
        }

        public IActionResult IndexAdmin()
        {
            return View(_commentServices.GetComments());
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Comment obj)
        {
            obj.UserId = _userManager.GetUserId(HttpContext.User);
            _commentServices.CreateComment(obj);
            TempData["success"] = "Comment Added successfully!";
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            if (_commentServices.CommentExists(id))
            {
                var comment = _commentServices.GetComment(id);
                TempData["success"] = "Comment delete successfully!";
                _commentServices.DeleteComment(comment);
                return RedirectToAction("IndexAdmin");
            }
            return NotFound();
        }
    }
}
