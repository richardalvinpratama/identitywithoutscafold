﻿using IdentityAppOnBoarding.Models;
using IdentityAppOnBoarding.Services.Contracts;
using IdentityAppOnBoarding.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IdentityAppOnBoarding.Controllers
{
    public class ProductController : Controller
    {
        private readonly ICategoryServices _categoryServices;
        private readonly IProductServices _productServices;
        private readonly IWebHostEnvironment _hostEnvironment;
        public ProductController(IProductServices productServices, ICategoryServices categoryServices, IWebHostEnvironment hostEnvironment)
        {
            _productServices = productServices;
            _categoryServices = categoryServices;
            _hostEnvironment = hostEnvironment;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult IndexAdmin()
        {
            ViewBag.LinkText = "Product";
            return View(_productServices.GetProducts());
        }

        public IActionResult Detail(int id)
        {
            ViewBag.LinkText = "Product";
            var product = _productServices.GetProduct(id);
/*            var categoryName = _categoryServices.GetCategory(product.CategoryId).Name;
            var productDetailVM = new ProductDetailViewModel
            {
                Id = product.Id,
                Name = product.Name,
                Desc = product.Desc,
                Stock = product.Stock,
                Price = product.Price,
                LastUpdate = product.LastUpdate,
                CategoryName = categoryName
            };*/
            return View(product);
        }

        public IActionResult Create()
        {
            ViewBag.LinkText = "Product";
            ViewBag.Categories = new SelectList(_categoryServices.GetCategories(), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Product obj)
        {
            //Save image to wwwroot/image
/*            string wwwRootPath = _hostEnvironment.WebRootPath;
            string fileName = Path.GetFileNameWithoutExtension(obj.ImageFile.FileName);
            string extension = Path.GetExtension(obj.ImageFile.FileName);
            obj.ImageName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            string path = Path.Combine(wwwRootPath, "/Image/", fileName);
            using(var fileStream = new FileStream(path, FileMode.Create))
            {
                await obj.ImageFile.CopyToAsync(fileStream);
            }*/
            //insert record
            _productServices.CreateProduct(obj);
            TempData["success"] = "Product Added successfully!";
            return RedirectToAction("IndexAdmin");
        }

        public async Task<IActionResult> Update(int id)
        {
            ViewBag.LinkText = "Product";
            if (_productServices.ProductExists(id))
            {
                var product = _productServices.GetProduct(id);
                var productVM = new ProductUpdateViewModel
                {
                    Id = product.Id,
                    Name = product.Name,
                    Desc = product.Desc,
                    Stock = product.Stock,
                    Price = product.Price,
                    CategoryId = product.CategoryId
                    /*CategoryLists = new SelectList(_categoryServices.GetCategories(), "Id", "Name");*/
                };
                /*return View(_productServices.GetProduct(id));*/
                return View(productVM);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(int id, ProductUpdateViewModel obj)
        {
            var product = new Product
            {
                Id = id,
                Name = obj.Name,
                Desc = obj.Desc,
                Stock = obj.Stock,
                Price = obj.Price,
                CategoryId = obj.CategoryId
            };
            _productServices.UpdateProduct(product);
            TempData["success"] = "Product update successfully!";
            return RedirectToAction("IndexAdmin");
        }

        public IActionResult Delete(int id)
        {
            if (_productServices.ProductExists(id))
            {
                var product = _productServices.GetProduct(id);
                TempData["success"] = "Product delete successfully!";
                _productServices.DeleteProduct(product);
                return RedirectToAction("IndexAdmin");
            }
            return NotFound();
        }



    }
}
