﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IdentityAppOnBoarding.Controllers
{
    [Authorize]
    public class AccessController : Controller
    {
        //Authorize from cookie/jwt
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Trainer,Pokemon")]
        public IActionResult PokemonAndTrainerAccess()
        {
            return View();
        }

        [Authorize(Policy = "OnlyTrainerChecker")]
        public IActionResult OnlyTrainerChecker()
        {
            return View();
        }
    }
}
