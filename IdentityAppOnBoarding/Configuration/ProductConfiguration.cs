﻿using IdentityAppOnBoarding.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IdentityAppOnBoarding.Configuration
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Product");
            builder.Property(c => c.Name)
                .IsRequired(true);
            builder.Property(c => c.Image).HasDefaultValue(null);

            builder.HasData
            (
                new Product
                {
                    Id = 1,
                    Name = "TV 100 Inch",
                    Desc = "TV yang sangat besar dan sudah 4K",
                    Stock = 20,
                    Price = 2000000,
                    CategoryId = 1
                },
                new Product
                {
                    Id = 2,
                    Name = "Baju Monster",
                    Desc = "Baju ini sangat mengerikan dan besar",
                    Stock = 10,
                    Price = 80000,
                    CategoryId = 2
                }
            );
        }
    }


}
