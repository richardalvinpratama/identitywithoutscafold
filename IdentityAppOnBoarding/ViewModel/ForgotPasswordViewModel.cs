﻿using System.ComponentModel.DataAnnotations;

namespace IdentityAppOnBoarding.ViewModel
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
