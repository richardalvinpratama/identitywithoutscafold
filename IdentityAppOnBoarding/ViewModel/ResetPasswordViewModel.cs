﻿using System.ComponentModel.DataAnnotations;

namespace IdentityAppOnBoarding.ViewModel
{
    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Passoword")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Password don't match!")]
        public string ConfirmPassword { get; set; }
        public string Code { get; set; }
    }
}
