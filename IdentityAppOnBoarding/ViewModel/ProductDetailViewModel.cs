﻿namespace IdentityAppOnBoarding.ViewModel
{
    public class ProductDetailViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public int Stock { get; set; }
        public double Price { get; set; }
        public DateTime LastUpdate { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
