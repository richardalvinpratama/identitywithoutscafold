﻿using IdentityAppOnBoarding.Data;
using IdentityAppOnBoarding.Models;
using IdentityAppOnBoarding.Services.Contracts;
using Microsoft.EntityFrameworkCore;

namespace IdentityAppOnBoarding.Services.Repositories
{
    public class CartServices : ICartServices
    {
        private ApplicationDbContext _context;
        public CartServices(ApplicationDbContext context)
        {
            _context = context;
        }
        public bool CartExists(int id)
        {
            return _context.Carts.Any(c => c.Id == id);
        }

        public IEnumerable<Cart> GetCartsbyUserId(string userId)
        {
            return _context.Carts.Include(e => e.Product).Include(e => e.AppUser).Where(e => e.UserId == userId).ToList();
        }

        public bool CreateCart(Cart cart)
        {
            _context.Add(cart);
            return Save();
        }

        public bool DeleteCart(Cart cart)
        {
            _context.Remove(cart);
            return Save();
        }

        public Cart GetCart(int id)
        {
            return _context.Carts.Where(e => e.Id == id).FirstOrDefault();
        }

        public IEnumerable<Cart> GetCarts()
        {
            return _context.Carts.Include(e => e.Product).Include(e => e.AppUser).ToList();
        }

        public bool UpdateCart(Cart cart)
        {
            _context.Update(cart);
            return Save();
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }
    }
}
