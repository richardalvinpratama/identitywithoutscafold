﻿using IdentityAppOnBoarding.Data;
using IdentityAppOnBoarding.Models;
using IdentityAppOnBoarding.Services.Contracts;
using Microsoft.EntityFrameworkCore;

namespace IdentityAppOnBoarding.Services.Repositories
{
    public class CommentServices : ICommentServices
    {
        private ApplicationDbContext _context;
        public CommentServices(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool CommentExists(int id)
        {
            return _context.Comments.Any(c => c.Id == id);
        }

        public bool CreateComment(Comment comment)
        {
            _context.Add(comment);
            return Save();
        }

        public bool DeleteComment(Comment comment)
        {
            _context.Remove(comment);
            return Save();
        }

        public IEnumerable<Comment> GetComments()
        {
            return _context.Comments.Include(x => x.AppUser).ToList();
        }

        public Comment GetComment(int id)
        {
            return _context.Comments.Where(e => e.Id == id).FirstOrDefault();
        }

        public bool UpdateComment(Comment comment)
        {
            throw new NotImplementedException();
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }
    }
}
