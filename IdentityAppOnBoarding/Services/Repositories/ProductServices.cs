﻿using IdentityAppOnBoarding.Data;
using IdentityAppOnBoarding.Models;
using IdentityAppOnBoarding.Services.Contracts;
using Microsoft.EntityFrameworkCore;

namespace IdentityAppOnBoarding.Services.Repositories
{
    public class ProductServices : IProductServices
    {
        private ApplicationDbContext _context;
/*        private readonly IMapper _mapper;*/
        public ProductServices(ApplicationDbContext context /*, IMapper mapper*/)
        {
            _context = context;
/*            _mapper = mapper;*/
        }
        public ICollection<Product> GetProducts()
        {
            return _context.Products.ToList();
        }

        public Product GetProduct(int id)
        {
            return _context.Products.Include(e => e.Category).Where(e => e.Id == id).FirstOrDefault();
        }

        public IQueryable<Product> GetProductbyCategory(int id)
        {
            var products = from p in _context.Products select p;
            return products;
            /*return _context.Products.Where(e => e.CategoryId == id).ToList();*/
        }

        public bool CreateProduct(Product product)
        {
            _context.Add(product);
            return Save();
        }

        public bool UpdateProduct(Product product)
        {
            _context.Update(product);
            return Save();
        }

        public bool DeleteProduct(Product product)
        {
            _context.Remove(product);
            return Save();
        }

        public bool ProductExists(int id)
        {
            return _context.Products.Any(c => c.Id == id);
        }

        public bool Save()
        {
            var saved = _context.SaveChanges();
            return saved > 0 ? true : false;
        }

    }
}
