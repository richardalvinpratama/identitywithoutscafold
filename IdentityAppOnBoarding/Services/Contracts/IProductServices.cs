﻿using IdentityAppOnBoarding.Models;

namespace IdentityAppOnBoarding.Services.Contracts
{
    public interface IProductServices
    {
        ICollection<Product> GetProducts();
        public Product GetProduct(int id);
        IQueryable<Product> GetProductbyCategory(int id);
        bool ProductExists(int id);
        bool CreateProduct(Product product);
        bool UpdateProduct(Product product);
        bool DeleteProduct(Product product);

    }
}
