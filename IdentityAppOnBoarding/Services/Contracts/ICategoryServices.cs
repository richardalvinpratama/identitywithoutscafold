﻿using IdentityAppOnBoarding.Models;

namespace IdentityAppOnBoarding.Services.Contracts
{
    public interface ICategoryServices
    {
        IEnumerable<Category> GetCategories();
        public Category GetCategory(int id);
/*        ICollection<Product> GetProductByCategory(int id);*/
        bool CategoryExists(int id);
        bool CreateCategory(Category category);
        bool UpdateCategory(Category category);
        bool DeleteCategory(Category category);

    }
}
