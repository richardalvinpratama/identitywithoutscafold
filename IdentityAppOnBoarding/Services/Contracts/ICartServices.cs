﻿using IdentityAppOnBoarding.Models;

namespace IdentityAppOnBoarding.Services.Contracts
{
    public interface ICartServices
    {
        IEnumerable<Cart> GetCarts();
        public Cart GetCart(int id);
        IEnumerable<Cart> GetCartsbyUserId(string userId);
        bool CartExists(int id);
        bool CreateCart(Cart cart);
        bool UpdateCart(Cart cart);
        bool DeleteCart(Cart cart);
    }
}
