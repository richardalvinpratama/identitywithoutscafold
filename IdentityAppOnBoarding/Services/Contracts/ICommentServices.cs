﻿using IdentityAppOnBoarding.Models;

namespace IdentityAppOnBoarding.Services.Contracts
{
    public interface ICommentServices
    {
        IEnumerable<Comment> GetComments();
        public Comment GetComment(int id);
        /*        ICollection<Product> GetProductByCategory(int id);*/
        bool CommentExists(int id);
        bool CreateComment(Comment comment);
        bool UpdateComment(Comment comment);
        bool DeleteComment(Comment comment);
    }
}
